﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EmployeeWebserviceREST
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeTest" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeTest.svc or EmployeeTest.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeTest : IEmployeeTest
    {
        EmployeeDataDataContext data = new EmployeeDataDataContext();

        public bool AddEmployee(Employee eml)
        {
            try
            {
                data.Employees.InsertOnSubmit(eml);
                data.SubmitChanges();
                return true;

            }
            catch
            {
                return false;
            }
        }



        public bool DeleteEmployee(int idE)
        {
            try
            {
                Employee employeeToDetele =
                    (from employee in data.Employees where employee.empID == idE select employee).Single();
                data.Employees.DeleteOnSubmit(employeeToDetele);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public List<Employee> GetProductList()
        {
            try
            {
                return (from employee in data.Employees select employee).ToList();
            }
            catch
            {
                return null;
            }
        }
        public bool UpdateEmployee(Employee eml)
        {
            Employee employeeToModify =
                (from employee in data.Employees where employee.empID == eml.empID select employee).Single();
            employeeToModify.age = eml.age;
            employeeToModify.address = eml.address;
            employeeToModify.firstName = eml.firstName;
            employeeToModify.lastName = eml.lastName;
            data.SubmitChanges();
            return true;
        }
    }
}

