﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication10.Models;

namespace WebApplication10.Context
{
    public class ProductContext : DbContext
    {
         public DbSet<Product> Products { get; set; }
    }
}